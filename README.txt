P.O.R.S Alkalmazás Telepítő


Tartalom
------------------------------------------------------------------------------------------------------------------------

Mappák:

db - 
   init.sql: létrehozza a kellő adatbázis táblákat, felhasználókat illetve adatbázist.
   postgresql.conf: adatbázis konfiguráció fájl.

docker-deb - A docker alkalmazás telepítő csomagjai.

scripts -
   docker-install.sh - A docker telepítő parancsfájl.
   install.sh - Az alkalmazás telepítő parancsfájl.
   start.sh - Elindítja a docker alkalmazást.
   stop.sh - Leállítja a docker alkalmazást.
   
Fájlok:

 app.jar - Az alkalmazás lefordított csomag.
 build.sh - A telepítő csomag építő parancsfájl.
 docker-compose.yml - A telepített alkalmazásnak docker paraméterei.
 DockerFile - A java jre és app.jar docker image fájl legyártáshoz.
 pors.app.tar.gz - A tömorített telepítő csomag.
 
 
Használat:

Futtassuk a build.sh parancsfájlt. Előtte fontos hogy a forráskódot tartalmazó mappa egy szinten
feljebb legyen a build.sh fájlhoz relatívan. Máshogyan mondva, a pors-deploy és a pors (forráskód) mappák
legyenek egymás melett. Ha ezt nem biztosítjuk akkor a build nem fog sikerülni!

pors/ pors-deploy/








