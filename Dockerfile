FROM openjdk:11-jre-slim-buster
RUN addgroup --system pors && adduser --system --ingroup pors  pors
USER pors:pors
ARG JAR_FILE=app.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
