#!/bin/bash


echo "***********************"
echo "P.O.R.S Docker Telepítő"
echo "***********************"
echo
echo "Docker telepítés..."
echo

sudo dpkg -i docker-deb/containerd.io_1.4.3-1_amd64.deb
sudo dpkg -i docker-deb/docker-ce-cli_20.10.2_3-0_debian-buster_amd64.deb
sudo dpkg -i docker-deb/docker-ce_20.10.2_3-0_debian-buster_amd64.deb

sudo cp docker-deb/docker-compose /usr/bin

echo
echo "Docker telepítés sikeres!"
echo


if id -nG "$USER" | grep -qw docker; then
    echo "Futtassa az installer.sh parancsfájlt a telepítés folytatáshoz."   
else
    sudo usermod -aG docker $USER
    echo "Lépjen ki a rendszerből és vissza. Ezt követően, futtassa az installer.sh parancsfájlt a telepítés folytatáshoz."
fi
