#!/bin/bash
target_dir=$1

if [ -z "$target_dir" ]; then
  echo "A cél könyvtár megadása kötelező!"
  exit 1
fi

if [ ! -d "$target_dir" ]; then
  echo "A megadott cél könyvtár nem könyvtár, nem talált vagy nem létezik!"
  exit 1
fi



echo "Alkalmazás image dockerba telepítése..."
echo

docker load < image/pors_app_latest.tar.gz

echo "Alkalmazás fájlok másolása -> $target_dir könyvtárba..."
echo

cp .env docker-compose.yml start.sh stop.sh $target_dir
cp -R db $target_dir

cd $target_dir

echo "Alkalmazás indul"

docker-compose up -d

cd -

echo
echo "Telepítés kész. Az alkalmazás jelenleg fut a háttérben."
echo
