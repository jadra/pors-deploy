#!/bin/bash

SRC_DIR=../pors
JAR_TARGET=${SRC_DIR}/target/pors.jar

rm -rf installer
mkdir -p installer installer/db installer/image installer/docker-deb

cp ${JAR_TARGET} app.jar

#build
docker build -t pors-app .

#compress image
docker save pors-app | gzip > installer/image/pors_app_latest.tar.gz



#copy files
cp ${SRC_DIR}/db/init.sql installer/db
cp ${SRC_DIR}/pors.env installer/.env
cp db/postgresql.conf installer/db
cp docker-compose.yml installer/
cp scripts/*.sh installer/
cp -R docker-deb installer/


#cp pors.env installer/.env
#cp postgresql.conf installer/


#package.sh

tar -czvf porsapp.tar.gz installer

rm -rf installer
