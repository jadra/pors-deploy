\set QUIET on
\set ON_ERROR_STOP on
set client_min_messages to warning;

\set db_name 'pors_prod'
\set pors_app_password `echo $PORS_APP_PASSWORD`

CREATE ROLE pors_app with login password :'pors_app_password';

CREATE DATABASE :db_name;
\c :db_name




create table departments
(
    id serial
        constraint departments_pk
            primary key,
    name varchar(255) not null
);

create unique index departments_name_uindex
    on departments (name);


create table suppliers
(
    id serial
        constraint suppliers_pk
            primary key,
    name varchar not null,
    contact_name varchar,
    contact_email varchar,
    contact_phone varchar,
    address text,
    city varchar,
    postal_code varchar
);

create unique index suppliers_name_uindex
    on suppliers (name);

create table users
(
    id serial not null,
    username varchar(255) not null primary key,
    password varchar(255) not null,
    enabled bool not null default true
);

create unique index users_id_index on users(id);
create index users_username_index on users (lower(username) varchar_pattern_ops);

create table authorities
(
    username varchar(255) not null,
    authority varchar(255) not null,
    constraint authorities_users_fk foreign key(username) references users(username)
);

create unique index authorities_username_uindex on authorities (username, authority);

create table profiles
(
    id serial
        constraint profiles_pk
            primary key,
    user_id int references users(id) on delete no action,
    department_id int references departments on delete restrict,
    last_name varchar(255),
    first_name varchar(255),
    phone1 varchar(50),
    phone2 varchar(255)
);

CREATE TABLE orders (
                        id serial constraint orders_pk primary key,
                        user_id int NOT NULL references profiles(id) on delete restrict,
                        supplier_id int NOT NULL references suppliers(id) on delete restrict,
                        status varchar NOT NULL default 'NEW',
                        created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        updated_at timestamp without time zone,
                        deleted_at timestamp without time zone
);

create index orders_status_index on orders (status);


CREATE TABLE order_items (
                             order_id int NOT NULL references orders(id) on delete restrict,
                             line_number int NOT NULL,
                             name character varying(255) NOT NULL,
                             description text,
                             quantity int NOT NULL default 1,
                             unit_price decimal(19,4) NOT NULL,
                             constraint pk_orderitem primary key (order_id, line_number)
);


grant select, insert, update, delete on table departments to pors_app;
grant select,update,usage on sequence departments_id_seq to  pors_app;
grant select, insert, update, delete on table suppliers to pors_app;
grant select, update, usage on sequence  suppliers_id_seq to pors_app;


grant select, insert, update, delete on table  users to pors_app;
grant select, update, usage on sequence users_id_seq to pors_app;
grant select, insert, update, delete on table  authorities to pors_app;

grant select, insert, update, delete on table profiles to pors_app;
grant select, update, usage on sequence profiles_id_seq to pors_app;

grant select, insert, update, delete on table orders to pors_app;
grant select, update, usage on sequence orders_id_seq to pors_app;

grant select, insert, update, delete on table order_items to pors_app;

insert into departments(name) values ('Informatika');